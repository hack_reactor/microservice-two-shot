import { useEffect, useState } from "react";

export default function ShoesForm() {
    const [bins, setBins] = useState([]);
    const fetchData = async () => {
        const binsUrl = "http://localhost:8100/api/bins/"
        const response = await fetch(binsUrl);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const [formChange, setFormChange] = useState({
        manufacturer: '',
        model: '',
        color: '',
        pictureUrl: '',
        bin: '',
    })

    const handleFormChange = (e) => {
        setFormChange({
            ...formChange,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        console.log("click")
        const d = {};
        d['manufacturer'] = formChange.manufacturer;
        d['model'] = formChange.model;
        d['color'] = formChange.color;
        d['picture_url'] = formChange.pictureUrl
        d['bin'] = formChange.bin;
        
        const shoeUrl = "http://localhost:8080/api/shoes/";
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(d),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(shoeUrl, fetchOptions);
        if (response.ok) {
            const data = await response.json()
            console.log(data);
            setFormChange({
                manufacturer: '',
                model: '',
                color: '',
                pictureUrl: '',
                bin: '',
            })
        }
    }

    return (
        <div className="my-5 container">
            <div className="offset-3 col-5">
                <form onSubmit={handleSubmit} className="">
                    <div className="shadow p-4 mt-4">
                        <h2>Add a new shoe</h2>
                        <div className="form mb-3" id='brand-field'>
                            <input onChange={handleFormChange} value={formChange.manufacturer} type="text" placeholder="Brand" name="manufacturer" className="form-control"></input>
                        </div>
                        <div className="form mb-3" id='model-field'>
                            <input onChange={handleFormChange} value={formChange.model} type="text" placeholder="Model" name="model" className="form-control"></input>
                        </div>
                        <div className="form mb-3" id='color-field'>
                            <input onChange={handleFormChange} value={formChange.color} type="text" placeholder="Color" name="color" className="form-control"></input>
                        </div>
                        <div className="form mb-3" id='picture-url-field'>
                            <input onChange={handleFormChange} value={formChange.pictureUrl} type="text" placeholder="Picture URL" name="pictureUrl" className="form-control"></input>
                        </div>
                        <div className="form mb-3" id='select-bin'>
                            <select onChange={handleFormChange} value={formChange.bin} name="bin" className="form-control">
                                <option value=''>Select a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>{bin.closet_name} - bin {bin.bin_number}</option>
                                        )
                                    })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Add shoe to closet</button>
                    </div>
                </form>
            </div>
        </div>
    );
};