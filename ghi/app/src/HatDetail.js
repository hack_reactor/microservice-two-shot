import React, {useEffect, useState} from 'react';
import { useParams } from 'react-router-dom';


function HatDetail() {
    const [hat, setHat] = useState([]);
    const {id} = useParams()

    const fetchData = async () => {
        const haturl = `http://localhost:8090/api/hats/${id}`;

        const response = await fetch(haturl);

        if (response.ok) {
            const data = await response.json();
            setHat(data.hat);
        }
    }

    const handleDelete = async () => {
        const hatUrl = `http://localhost:8090/api/hats/${id}`;
        const fetchConfig = {
            method: "delete",
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            console.log("item successfully deleted");
        } else {
            console.log("error in deletion")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);
    if (hat.locations === undefined) {
        return null
    } else {

            return (
                <>
            <div className="col">
                <div key={hat.href} className="card mb-3 shadow" style={{width:'18rem'}}>
                    <img src={hat.pictureUrl} className="card-img-top"/>
                    <div className="card-body">
                        <h2 className="card-title">Item Name: {hat.item_name}</h2>
                        <h3 className="card-title">Fabric: {hat.fabric}</h3>
                        <h4 className="card-title">Style: {hat.style_name}</h4>
                        <h5 className="card-title">color: {hat.color}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">
                            <div>{hat.locations.closet_name}</div>
                        </h6>
                    </div>
                    <div className="card-footer">
                    Section number:
                    {hat.locations.section_number}
                    /
                    Shelf number:
                    {hat.locations.shelf_number}
                    </div>
                    <div>
                        <button onClick={handleDelete}>Delete</button>
                    </div>
                </div>
            </div>
            </>
        )
    }

}

export default HatDetail;
