import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListShoes from './ListShoes';
import ShoeDetail from './ShoeDetail';
import ShoesForm from './ShoesForm';
import HatsList from './HatsList';
import HatDetail from './HatDetail';
import HatForm from './HatForm'


function App() {
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/shoes">
            <Route index element={<ListShoes />} />
            <Route path=":id" element={<ShoeDetail />} />
            <Route path="create" element={<ShoesForm />} />
          </Route>
          <Route path='/hats'>
            <Route index element={<HatsList />} />
            <Route path=':id' element={<HatDetail/>} />
            <Route path='create' element={<HatForm/>} />
          </Route>
          <Route path='*' element={<p>sorry, you have the wrong page.</p>}/>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
