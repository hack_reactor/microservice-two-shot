import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const ListShoes = () => {
    const [shoes, setShoes] = useState([])
    const fetchData = async () => {
        const response = await fetch("http://localhost:8080/api/shoes/");
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <>
        <div className="container">
            <h1>Shoes</h1>
            <Link to="/shoes/create">Add a shoe</Link>
            <div className="container row row-cols-auto">
            {shoes.map(shoe => {
                return (
                    <Link key={shoe.href} to={`/shoes/${shoe.id}`} style={{textDecoration: "none", color: "black"}}>
                        <div className="card shadow" style={{width: "18rem", margin: "10px"}}>
                            <img src={shoe.picture_url} className="card-img-top" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">{shoe.model}</h5>
                                <h6 className="card-subtitle">{shoe.manufacturer}</h6>
                                <p className="card-text">{shoe.color}</p>
                            </div>
                        </div>
                    </Link>
                );
            })}
            </div>
        </div>
        </>
    )
}

export default ListShoes;
