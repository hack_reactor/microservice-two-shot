import React, {useEffect, useState} from 'react';

function HatForm() {
    const [locations, setLocations] = useState([]);
    const [item_name, setItemName] = useState('');
    const [fabric, setFabric] = useState('');
    const [style_name, setSytleName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setSytleName(value);
    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleItemChange = (event) => {
        const value = event.target.value;
        setItemName(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)
        }
    }

    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {};
        data.item_name = item_name;
        data.fabric = fabric;
        data.style_name = style_name;
        data.color = color;
        data.pictureUrl = pictureUrl;
        data.locations = location;

        console.log(data)

        const hatUrl = `http://localhost:8090/api/hats/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setItemName('');
            setFabric('');
            setSytleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
          {/* <div className="col col-sm-auto">
            <img width="600" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
          </div> */}
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a hat</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleItemChange} placeholder="Item Name" name='item_name' value={item_name} type="text" id="item_name" className="form-control"/>
                <label htmlFor="item_name">Item Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} placeholder="Fabric" name='fabric' value={fabric} type="text" id="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange} placeholder="Style" name='style_name' value={style_name} type="text" id="style_name" className="form-control"/>
                <label htmlFor="style_name">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="Color" name='color' value={color} type="text" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} placeholder="Picture Url" name='pictureUrl' value={pictureUrl} type="url" id="PictureUrl" className="form-control"/>
                <label htmlFor="pictureUrl">Picture Url</label>
              </div>
              <div className="mb-3">
                <select name="location" onChange={handleLocationChange} value={location} id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.href} value={location.href}>
                      {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default HatForm;
