import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';


function HatsList() {
  const [hats, setHats] = useState([])
  const fetchData = async () => {
    const hatsUrl = 'http://localhost:8090/api/hats/';
    const response = await fetch(hatsUrl);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }
  useEffect(() => {
    fetchData();
  }, [])
  return (
    <>
    <h1>Hats</h1>
    <Link to={`/hats/create`}>Add Hat</Link>
    <div className="col">
      {hats.map(hat => {
        return (
          <Link to={`${hat.id}`} key={hat.href}>
          <div className="card mb-3 shadow" style={{width:'18rem', display:'-webkit-inline-flex'}}>
            <img src={hat.pictureUrl} className="card-img-top" />
            <div className="card-body">
              <h2 className="card-title">Item Name: {hat.item_name}</h2>
              <h3 className="card-title">Fabric: {hat.fabric}</h3>
              <h4 className="card-title">Style: {hat.style_name}</h4>
              <h5 className="card-title">color: {hat.color}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {hat.locations.closet_name}
              </h6>
            </div>
            <div className="card-footer">
              Section number:
              {hat.locations.section_number}
              /
              Shelf number:
              {hat.locations.shelf_number}
            </div>
          </div>
          </Link>
        );
      })}
    </div>
    </>
  );
}

export default HatsList;
