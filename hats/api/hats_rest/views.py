from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# from .acls import get_photo

# Create your views here.

from common.json import ModelEncoder
from .models import Hats, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "item_name",
    ]

    def get_extra_data(self, o):
        return {"locations": o.locations.closet_name}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "item_name",
        "fabric",
        "style_name",
        "color",
        "pictureUrl",
        "locations",
    ]
    encoders = {
        "locations": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, locationVoHref=None):
    if request.method == "GET":
        if locationVoHref is not None:
            hats = Hats.objects.filter(location=locationVoHref)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsDetailEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["locations"]
            location = LocationVO.objects.get(import_href=location_href)
            content["locations"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(
            {"hat": hat},
            encoder=HatsDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        try:
            location_href = content["locations"]
            location = LocationVO.objects.get(import_href=location_href)
            content["locations"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hats.objects.update(**content)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
