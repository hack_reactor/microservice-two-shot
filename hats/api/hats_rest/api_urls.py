from django.urls import path

from .views import api_list_hats, api_show_hat

urlpatterns = [
    # path("createhats/", api_list_hats, name="api_create_hats"),
    path("hats/", api_list_hats, name="api_list_hat"),
    path("hats/<int:id>", api_show_hat, name="api_show_hat")
]
