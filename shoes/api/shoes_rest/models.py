from django.db import models
from django.urls import reverse


class BinsVO(models.Model):
    import_href = models.CharField(max_length=200, null=True, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100, null=True)
    model = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=50, null=True)
    picture_url = models.URLField(max_length=1000, blank=True, null=True)
    bin = models.ForeignKey(
        BinsVO,
        related_name="shoe",
        on_delete=models.PROTECT,
        null=True,
    )

    def get_api_url(self):
        return reverse("shoe_details", kwargs={"pk": self.pk})
