from .keys import pexel_api
import requests
import json


def get_photo(brand, model):
    headers = {"Authorization": pexels_api}
    params = {
        "per_page": 1,
        "query": f"{brand} {model}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        return {"picture_url": None}
