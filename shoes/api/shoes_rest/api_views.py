from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinsVO, Shoe

# from .acls import get_photo


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model",
        "color",
        "picture_url",
        # "bin",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model",
        "color",
        "picture_url",
        # "bin",
    ]

    def get_extra_data(self, o):
        return {
            "bin": {
                "bin": o.bin.bin_number,
                "closet_name": o.bin.closet_name,
            }
        }


class BinsEncoder(ModelEncoder):
    model = BinsVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
    ]


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bins_vo_id=None):
    if request.method == "GET":
        if bins_vo_id is None:
            shoes = Shoe.objects.all()
        else:
            shoes = Shoe.objects.filter(bin=bins_vo_id)
        return JsonResponse(
            {"shoes": shoes},
            ShoeEncoder,
            False,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinsVO.objects.get(import_href=bin_href)
            content["bin"] = bin
            # content["picture_url"] = get_photo(
            #     content["manufacturer"], content["model"]
            # )
        except BinsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                satuts=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT", "GET"])
def api_shoe_details(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse({"shoe": shoe}, ShoeDetailEncoder, False)
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin_href = content["bin"]
                bin = BinsVO.objects.get(import_href=bin_href)
                content["bin"] = bin
        except BinsVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.update(**content)
        return JsonResponse(
            {"shoe": shoe},
            ShoeDetailEncoder,
            False,
        )
