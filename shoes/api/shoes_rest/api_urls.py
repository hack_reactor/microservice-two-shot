from django.urls import path
from .api_views import api_list_shoes, api_shoe_details

urlpatterns = [
    path("shoes/", api_list_shoes, name="add_shoe"),
    path("bins/<int:bins_vo_id>/shoes/", api_list_shoes, name="list_shoes"),
    path("shoes/<int:pk>/", api_shoe_details, name="shoe_details"),
]
