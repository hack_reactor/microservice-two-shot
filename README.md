# Wardrobify

Team:

- Josh - Shoes
- Toon - Hats

## Design

## Shoes microservice

Includes 2 models, a bin value-object model, which polls bins from a wardrobe application, and a shoe model, with brand (manufacturer), model, color, photo, and bin (location) fields. Provides api endpoints for a react front end to display a list of all shoes, along with details of each shoe.

## Hats microservice

Includes 2 models, a location value-object model, which polls locations from a wardrobe application, and a hat model, with item name, fabric, style name, color, photo, and a location field. Provides api endpoints for a react front end to display a list of all hats, along with details of each hat. in the detail of each hat is a delete button. on the hat list an add hat form link was added.
